<?php
require_once 'database.php';

function CreateUser($nom, $prenom, $email, $motDePasse, $salt, $signaler){
    try{
    $sql = "INSERT INTO gepatisse.utilisateurs(nom, prenom, email, motDePasse, salt, signaler) 
    VALUES (:nom, :prenom, :email, :motDePasse, :salt, :signaler);";
    $query = connect()->prepare($sql);
    
    $query->execute([
        ':nom' => $nom,
        ':prenom' => $prenom,
        ':email' => $email,
        ':motDePasse' => $motDePasse,
        ':salt' => $salt,
        ':signaler' => $signaler,
    ]);

    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

function CreateConfectionner($nom, $prenom, $email, $motDePasse, $salt, $signaler, $statut, $lat, $long){
    try{
    $sql = "INSERT INTO gepatisse.patissiers(nom, prenom, email, motDePasse, salt, signaler, statut, lat, long) 
    VALUES (:nom, :prenom, :email, :motDePasse, :salt, :signaler, :statut, :lat, :long);";
    $query = connect()->prepare($sql);
    
    $query->execute([
        ':nom' => $nom,
        ':prenom' => $prenom,
        ':email' => $email,
        ':motDePasse' => $motDePasse,
        ':salt' => $salt,
        ':signaler' => $signaler,
        ':statut' => $statut,
        ':lat' => $lat,
        ':long' => $long,
    ]);

    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

function CreateMessage($titre, $datePublication, $dateModification, $contenu, $nomUser){
    try{
    $sql = "INSERT INTO gepatisse.messages(nom, prenom, email, motDePasse, salt, signaler, statut, lat, long) 
    VALUES (:nom, :prenom, :email, :motDePasse, :salt, :signaler, :statut, :lat, :long);";
    $query = connect()->prepare($sql);
    
    $query->execute([
        ':nom' => $nom,
        ':prenom' => $prenom,
        ':email' => $email,
        ':motDePasse' => $motDePasse,
        ':salt' => $salt,
        ':signaler' => $signaler,
        ':statut' => $statut,
        ':lat' => $lat,
        ':long' => $long,
    ]);

    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}