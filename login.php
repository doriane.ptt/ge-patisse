<?php
/*
 *  Auteur  :   Doriane, Thi-kim, Pablito
 *  Classe  :   P3A
 *  Date    :   2020/02/03
 *  Desc.   :   page de login de l'application
*/

session_start();

if (!isset($_SESSION)) {
    require_once "./php/crud_user.php";

    $errors = array();
    $passwd = filter_input(INPUT_POST, 'passwd', FILTER_SANITIZE_STRING);
    $mail = filter_input(INPUT_POST, 'mail', FILTER_SANITIZE_EMAIL);
    $btn = filter_input(INPUT_POST, 'submit');

    if ($btn == 'connect') {
        // vérification de la présence de l'adresse mail dans la database
    } else {
        # code...
    }
} else {
    header('Location: ./index.php');
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body>
    <h1>Login/Logout</h1>
    <?php include_once 'nav.inc.php'; ?>
</body>
</html>