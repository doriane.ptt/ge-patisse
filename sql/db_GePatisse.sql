-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema GePatisse
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema GePatisse
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `GePatisse` DEFAULT CHARACTER SET utf8mb4 ;
USE `GePatisse` ;

-- -----------------------------------------------------
-- Table `GePatisse`.`Patisseries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GePatisse`.`Patisseries` (
  `idPatisserie` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  `prix` VARCHAR(45) NOT NULL,
  `image` VARCHAR(45) NULL,
  PRIMARY KEY (`idPatisserie`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GePatisse`.`Personnes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GePatisse`.`Personnes` (
  `idPeersonne` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  `prenom` VARCHAR(45) NOT NULL,
  `motDePasse` VARCHAR(45) NOT NULL,
  `salt` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idPeersonne`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GePatisse`.`Utilisateurs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GePatisse`.`Utilisateurs` (
  `email` VARCHAR(45) NOT NULL,
  `signaler` TINYINT NOT NULL,
  `Personnes_idPeersonne` INT NOT NULL,
  PRIMARY KEY (`Personnes_idPeersonne`),
  CONSTRAINT `fk_Utilisateurs_Personnes`
    FOREIGN KEY (`Personnes_idPeersonne`)
    REFERENCES `GePatisse`.`Personnes` (`idPeersonne`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GePatisse`.`Patissiers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GePatisse`.`Patissiers` (
  `statut` VARCHAR(45) NOT NULL,
  `lat` DOUBLE NOT NULL,
  `long` DOUBLE NOT NULL,
  `Utilisateurs_Personnes_idPeersonne` INT NOT NULL,
  PRIMARY KEY (`Utilisateurs_Personnes_idPeersonne`),
  CONSTRAINT `fk_Patissiers_Utilisateurs1`
    FOREIGN KEY (`Utilisateurs_Personnes_idPeersonne`)
    REFERENCES `GePatisse`.`Utilisateurs` (`Personnes_idPeersonne`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GePatisse`.`Commandes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GePatisse`.`Commandes` (
  `idCommande` INT NOT NULL AUTO_INCREMENT,
  `dateCommande` TIMESTAMP(100) NULL,
  `Patissiers_Utilisateurs_Personnes_idPeersonne` INT NOT NULL,
  PRIMARY KEY (`idCommande`, `Patissiers_Utilisateurs_Personnes_idPeersonne`),
  INDEX `fk_Commandes_Patissiers1_idx` (`Patissiers_Utilisateurs_Personnes_idPeersonne` ASC),
  CONSTRAINT `fk_Commandes_Patissiers1`
    FOREIGN KEY (`Patissiers_Utilisateurs_Personnes_idPeersonne`)
    REFERENCES `GePatisse`.`Patissiers` (`Utilisateurs_Personnes_idPeersonne`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GePatisse`.`Administrateurs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GePatisse`.`Administrateurs` (
  `Personnes_idPeersonne` INT NOT NULL,
  PRIMARY KEY (`Personnes_idPeersonne`),
  CONSTRAINT `fk_Administrateurs_Personnes1`
    FOREIGN KEY (`Personnes_idPeersonne`)
    REFERENCES `GePatisse`.`Personnes` (`idPeersonne`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GePatisse`.`Messages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GePatisse`.`Messages` (
  `idMessage` INT NOT NULL AUTO_INCREMENT,
  `titre` VARCHAR(45) NULL,
  `contenu` VARCHAR(200) NOT NULL,
  `datePublication` TIMESTAMP(100) NULL,
  `dateModification` DATETIME(100) NULL,
  `Utilisateurs_Personnes_idPeersonne` INT NOT NULL,
  PRIMARY KEY (`idMessage`, `Utilisateurs_Personnes_idPeersonne`),
  INDEX `fk_Messages_Utilisateurs1_idx` (`Utilisateurs_Personnes_idPeersonne` ASC),
  CONSTRAINT `fk_Messages_Utilisateurs1`
    FOREIGN KEY (`Utilisateurs_Personnes_idPeersonne`)
    REFERENCES `GePatisse`.`Utilisateurs` (`Personnes_idPeersonne`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GePatisse`.`produit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GePatisse`.`produit` (
  `Patisseries_idPatisserie` INT NOT NULL,
  `Patissiers_Utilisateurs_Personnes_idPeersonne` INT NOT NULL,
  PRIMARY KEY (`Patisseries_idPatisserie`, `Patissiers_Utilisateurs_Personnes_idPeersonne`),
  INDEX `fk_Patisseries_has_Patissiers_Patissiers1_idx` (`Patissiers_Utilisateurs_Personnes_idPeersonne` ASC),
  INDEX `fk_Patisseries_has_Patissiers_Patisseries1_idx` (`Patisseries_idPatisserie` ASC),
  CONSTRAINT `fk_Patisseries_has_Patissiers_Patisseries1`
    FOREIGN KEY (`Patisseries_idPatisserie`)
    REFERENCES `GePatisse`.`Patisseries` (`idPatisserie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Patisseries_has_Patissiers_Patissiers1`
    FOREIGN KEY (`Patissiers_Utilisateurs_Personnes_idPeersonne`)
    REFERENCES `GePatisse`.`Patissiers` (`Utilisateurs_Personnes_idPeersonne`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GePatisse`.`comporte`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GePatisse`.`comporte` (
  `Patisseries_idPatisserie` INT NOT NULL,
  `Commandes_idCommande` INT NOT NULL,
  `Commandes_Patissiers_Utilisateurs_Personnes_idPeersonne` INT NOT NULL,
  PRIMARY KEY (`Patisseries_idPatisserie`, `Commandes_idCommande`, `Commandes_Patissiers_Utilisateurs_Personnes_idPeersonne`),
  INDEX `fk_Patisseries_has_Commandes_Commandes1_idx` (`Commandes_idCommande` ASC, `Commandes_Patissiers_Utilisateurs_Personnes_idPeersonne` ASC),
  INDEX `fk_Patisseries_has_Commandes_Patisseries1_idx` (`Patisseries_idPatisserie` ASC),
  CONSTRAINT `fk_Patisseries_has_Commandes_Patisseries1`
    FOREIGN KEY (`Patisseries_idPatisserie`)
    REFERENCES `GePatisse`.`Patisseries` (`idPatisserie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Patisseries_has_Commandes_Commandes1`
    FOREIGN KEY (`Commandes_idCommande` , `Commandes_Patissiers_Utilisateurs_Personnes_idPeersonne`)
    REFERENCES `GePatisse`.`Commandes` (`idCommande` , `Patissiers_Utilisateurs_Personnes_idPeersonne`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;



SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
